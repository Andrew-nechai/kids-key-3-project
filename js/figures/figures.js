class additionalFeatures extends PIXI.Graphics {
    constructor() {
        super();
    }
}


export class createDot extends additionalFeatures {
    constructor(_cellsize) {
        super();
        this.beginFill(0x000000);
        this.drawCircle(0, 0, _cellsize/6);
        this.endFill();
        this.figtype = "point";
    }
}

export class createHorisLine extends PIXI.Graphics {
    constructor(_cellsize) {
        super();
        this.beginFill(0x000000);
        this.drawRect(0, 0, _cellsize, _cellsize/10);
        this.pivot.set(_cellsize/2, _cellsize/20);
        this.endFill();
        this.figtype = "hline";
    }
}

export class createVertLine extends PIXI.Graphics {
    constructor(_cellsize) {
        super();
        this.beginFill(0x000000);
        this.drawRect(0, 0, _cellsize/10, _cellsize);
        this.pivot.set(_cellsize/20, _cellsize/2);
        this.endFill();
        this.figtype = "vline";
    }
}