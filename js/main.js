let dotes = [
    //1
    // {x: 25, y: 0},
    // {x: 10, y: 10},
    // {x: 20, y: 20},
    // {x: 30, y: 20},
    // {x: 40, y: 10},
    // {x: 25, y: 10}

    //2    
    // {x: 100, y: 10},
    // {x: 20, y: 20},
    // {x: 30, y: 20},
    // {x: 40, y: 10},
    // {x: 0, y: 10},
    // {x: 25, y: 0}

    //3
    {x: 300, y: 300},
    {x: 500, y: 300},
    {x: 500, y: 500},

    //4
    // {x: 500, y: 300},
    // {x: 460, y: 480},
    // {x: 500, y: 500},

    //5
    // {x: 500, y: 500},
    // {x: 200, y: 300},
    // {x: 500, y: 300},
];

function calcLineEquation(dotes) {
    let k = 0;
    let c = 0;
    let b = 0;
    let onLine = true;

    let angle = 0.05;
    let countDotesOnTopSide = 0;
    let countDotesOnBottomSide = 0;
    let flag = true;
    let isAnyDotOnLine = false;
    let j = 0;

    if (dotes.length % 2 == 0) {
        let sortedDotes = dotes.sort(sortByX).sort(sortByY);
        let middleDote = {};
                
        let coordsX1 = sortedDotes[sortedDotes.length/2-1].x;
        let coordsY1 = sortedDotes[sortedDotes.length/2-1].y;
        let coordsX2 = sortedDotes[sortedDotes.length/2].x;
        let coordsY2 = sortedDotes[sortedDotes.length/2].y;

        let differenceX = Math.abs(coordsX1 - coordsX2)/2;
        let differenceY = Math.abs(coordsY1 - coordsY2)/2;
        let minX = Math.min(coordsX1, coordsX2);
        let minY = Math.min(coordsY1, coordsY2);

        middleDote.x = differenceX + minX;
        middleDote.y = differenceY + minY;

        b = middleDote.y;
        c = middleDote.x;
    }
    else {
        let mainDot = dotes[0];
        let mainDotX = mainDot.x;
        let mainDotY = mainDot.y;
        c = mainDotX;
        b = mainDotY;
        
        checkAllDotes(k, c, b);
        
    }

    function checkAllDotes() {
        while (flag) {
            countDotesOnTopSide = countDotesOnBottomSide = 0;
            isAnyDotOnLine = false;
            for (let i=1; i<dotes.length; i++) {
                let side = calcY(k, c, b, dotes[i].x, dotes[i].y);
                if (side == 1) {
                    countDotesOnTopSide++;
                }
                if (side == -1) {
                    countDotesOnBottomSide++;
                }
                if (side == 0) {
                    isAnyDotOnLine = true;
                }
            }

            if ((countDotesOnBottomSide == countDotesOnTopSide) && !isAnyDotOnLine) {
                flag = false;
            } else {
                flag = true;
                if (k >= 20 || k <= -20) {
                    angle = -angle;
                    k = 0;
                }
                k += angle;
            }
            j++
        }        
    }

    //ax+by+c=0
    let cofA = k;
    let digitC = k * (-c) + b;

    console.log(cofA, digitC)

    return [-cofA, -digitC];
}

function sortByX(a, b) {
    if (a.x > b.x) return 1;
    if (a.x == b.x) return 0;
    if (a.x < b.x) return -1;
}

function sortByY(a, b) {
    if (a.y < b.y) return 1;
    if (a.y == b.y) return 0;
    if (a.y > b.y) return -1;
}

function calcY(k, c, b, x, y) {
    let rez = k*(x-c)+b;
    if (rez > y) {
        return 1;
    }
    if (rez == y) {
        return 0;
    }
    if (rez < y) {
        return -1
    }
}

function isDotOnLine(x, y, k, c, b) {
    if (y == k*(x-c)+b)  return true;
    return false;
}

createCanvas(1200, 600, dotes);

function createCanvas(rectWidth, rectHeight, dotes){
    const canvas = document.getElementById("myapp");

    let app = new PIXI.Application({ 
        view: canvas,
        width: rectWidth,         // default: 800
        height: rectHeight,        // default: 600
        antialias: true,    // default: false
        transparent: false, // default: false
        resolution: 1       // default: 1
    });

    let zeroPoint = {
        x: 100,
        y: 100
    }


    dotes.forEach(element => {
        let dot = new drawDot(element.x + zeroPoint.x, element.y + zeroPoint.y);
        app.stage.addChild(dot);
    });

    let dataLineMass = calcLineEquation(dotes);

    let coffA = dataLineMass[0];
    let coffB = 1;
    let digitC = dataLineMass[1];
    let line = new drawLine(coffA, coffB, digitC, zeroPoint);
    app.stage.addChild(line);
}

function drawDot(x, y) {
    let circle = new PIXI.Graphics();
    circle.beginFill(0xFFFFFF);
    circle.drawCircle(0, 0, 2);
    circle.endFill();
    circle.x = x;
    circle.y = y;
    return circle;
}

function drawLine(a, b, c, zeroPoint) {
    console.log(a, b, c)
    let line = new PIXI.Graphics();
    line.lineStyle(1, 0xFFFFFF);
    if (a >= 0){
        line.moveTo(-100,  -c+(100)*a)
        line.lineTo(1000, -c+(-1000)*a)
    } else {
        line.moveTo(1000,  -c+(-1000)*a)
        line.lineTo(-100, -c+(100)*a)
    }

    line.x = zeroPoint.x;
    line.y = zeroPoint.y;
    return line;
}
